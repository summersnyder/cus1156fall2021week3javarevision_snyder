package cus1156.catlab2;

import java.util.ArrayList;

/**
 * This class represents a group of cats. The cats are created when the CatManager is created
 * although more cats can be added later.
 * 
 */
public class CatManager
{
	private ArrayList<Cat> myCats;
	public CatManager()
	{
		myCats = new ArrayList<>();
		Cat cat = new Cat("Fifi", "black");
		myCats.add(cat);
		cat = new Cat("Fluffy", "spotted");
		myCats.add(cat);
		cat = new Cat("Josephine", "tabby");
		myCats.add(cat);
		cat = new Cat("Biff", "tabby");
		myCats.add(cat);
		cat = new Cat("Bumpkin", "white");
		myCats.add(cat);
		cat = new Cat("Spot", "spotted");
		myCats.add(cat);
		cat = new Cat("Lulu", "tabby");
		myCats.add(cat);
	}

	// add a cat to the group
	public void add(Cat aCat)
	{
		myCats.add(aCat);
	}
	
	/**returns the first cat whose name matches, or null if no cat
                    // with that name is found
                     **/
	public Cat findThisCat(String name)
	{
		for (int i = 0; i < myCats.size(); i++)
		{
			Cat cat = myCats.get(i);
			if (cat.getName().equals(name))
			{
			return cat;
			}
		}
		return null;
	}

	// returns the number of cats of this color
	public int countColors(String color)
	{
		int count = 0;
		for (int i = 0; i < myCats.size(); i++)
		{
			Cat cat = myCats.get(i);
	    	if (cat.getColor().equals(color))
	    	{
	    		count++;
	    	}
	    }
	    return count;
	}
}